# Soldering Assignment

This is a continuation of the [CAD assignment](/kicad/index.md).

This assignment will show you how to populate PCBs with surface-mount and through-hole components, how to assemble an enclosure with panel-mount parts, and will cover the basics of programming a bare AVR chip.  At the end of the assignment, you will have a completed encabulator and will demonstrate its function to the TA.

Obtain the parts from the TA and make sure you aren't missing anything:

- 3x 1k resistors
- 2x 1uF capacitors
- 1x LM1117 5V regulator
- 1x ATTiny85
- 1x ISP header
- 3x 2 pin headers
- 1x 3 pin headers
- 1x panel mount LED
- 1x 2 pin MTA100 connector


![Unpopulated PCB](parts.jpg ':size=350')
![Components](parts2.jpg ':size=350')

Skip to the [programming](#programming) section after populating your board if you are already familiar with surface-mount soldering.

## Soldering

**Poor soldering is the cause of 99% of problems for this assignment**.  Be sure to watch the videos to learn how to make good solder joints and carefully inspect your joints afterwards.

We'll begin by populating the surface-mount components first.  Many people use a method known as [reflow soldering](https://www.youtube.com/watch?v=DYrucIWig24), where solder paste in a syringe is applied to each pad, components are placed on the paste, and the whole board is heated in an oven or hotplate.

Instead, we'll be using a regular soldering iron for populating our boards.  This is only possible if the components are large enough and have exposed pads, so keep this in mind before choosing components in [QFP](https://en.wikipedia.org/wiki/Quad_flat_package) or [BGA](https://en.wikipedia.org/wiki/Ball_grid_array) packages.

Start by wetting your sponge and cleaning the hot tip of your iron off.  Apply a small bead of solder to your tip to help transfer heat from the iron to the part being soldered.

![Dry solder tip](solder_tip.jpg ':size=350')
![Bead of solder on tip](solder_tip2.jpg ':size=350')

To start off, we're going to "tack" each of our components to the board with one of their pins to hold them in place.  Add a little bit of solder to just one pad of each part as in the picture below.  Then use the tweezers and iron to tack down the microcontroller.

![Solder applied to one pad on each part](solder_tack.jpg ':size=x250')
![Tacking the ATTiny85 into place](solder_tack2.jpg ':size=x250')

Keep these three steps in your head when soldering pins (see video below):

1. Put your iron in contact with both the leg of the component and the pad.
2. Apply some solder to the point of contact between the iron and the pad.
3. Leave the iron in contact with the pad for 1-2s to allow the solder to "soak" in.

 <video width="640" height="480" controls>
 <source src="/ece445/wiki/soldering/surfacemount.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 

<iframe width="600" height="480" src="https://www.youtube.com/embed/f9fbqks3BS8?start=436" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<!-- ([demo video](https://youtu.be/OaOaRaGGdMc?t=858)) -->

It's important that the IC is flat when you've tacked it down or it may be difficult to get all pads connected.  Repeat this for the other surface-mount parts.

![Verifying that IC is flat](solder_tack3.jpg ':size=350x250')
![All components are tacked down](solder_tack4.jpg ':size=350x250')

<!-- ![](solder_cleanup.jpg ':size=350') -->
<!-- ![](solder_cleanup2.jpg ':size=350') -->
<!-- ![](solder_cleanup3.jpg) -->

Solder the components fully after they are tacked down.  The solder joint should be shiny and cover the pad completely.  Pay attention to the orientation of the diode.

![Schottky diode orientation](diode.jpg ':size=x250')
![Pin 4 needs more solder](solder_cleanup4.png ':size=x250')

Do the same for the through-hole headers by adding solder to just one of the pads for each part.  Clamp the PCB in the vise, and while heating the soldered pad, push the header through.  Be sure to get the connector orientation right before soldering.

![](solder_conn.jpg ':size=350x250')
![](solder_conn3.jpg ':size=350x250')

Check that the component is flat and repeat this for the other headers.

![](solder_conn_flat.jpg ':size=350x250')
![](solder_conn_flat2.jpg ':size=350x250')


<!-- ![](solder_conn2.jpg ':size=350') -->
<!-- ![](solder_conn4.jpg) -->

Like before, these solder joints should be shiny and cover the whole pad.

<!-- ![Pin 1 needs more solder](solder_conn_clean.png ':size=450') -->

 <video width="640" height="480" controls>
 <source src="/ece445/wiki/soldering/good_throughhole.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 

 <video width="640" height="480" controls>
 <source src="/ece445/wiki/soldering/goodbad_surfacemount.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 

## Programming

Plug the USBasp programmer into your computer and your board and follow the `Configuration` instructions on the [AVR](/microcontrollers/avr) page.

![](programming.jpg)

``` c
// Test sketch

#define BUT_PIN PIN_PB4
#define LED_PIN PIN_PB2
#define POT_PIN A3

void setup() {
  pinMode(LED_PIN, OUTPUT);
  pinMode(BUT_PIN, INPUT);
  // enable internal pullup
  digitalWrite(BUT_PIN, HIGH);
}

void loop() {
  // read potentiometer and rescale to 0-1s delay
  int delay_time = 1000 * int32_t(analogRead(POT_PIN)) / 1024;

  // blink while button depressed
  if (digitalRead(BUT_PIN) == LOW) {
    digitalWrite(LED_PIN, HIGH);
    delay(delay_time);
    digitalWrite(LED_PIN, LOW);
    delay(delay_time);
  } else {
    digitalWrite(LED_PIN, LOW);
  }

}
```

If the board is successfully programmed, you will see a message like below.

![](success.png)

## Assembly

Obtain these parts from your TA.  These parts will be returned after the assignment is complete, so do not damage them.


- 1x button w/ nut
- 1x potentiometer w/ nut
- 1x barrel jack w/ nut


- 1x USBASP programmer with adapter
- 1x barrel plug to USB cable
- 1x enclosure w/ screws


![Prefabricated panel mount components](parts3.jpg ':size=350')
![Programmer and barrel plug cable](parts4.jpg ':size=350')
![Enclosure](parts5.jpg ':size=350')

Cut the wire leads of the LED to about 3 inches in length and insert it into the enclosure **(do this before crimping the connector)**.  The wire leads should not be stripped.

Using the vise, clamp the MTA-100 connector into place and use the crimping tool on the edge of your PCB to push the positive lead of the LED into position 1 of the connector.  This may require some force.


![](crimp.jpg ':size=x400')
![](crimp2.jpg ':size=x400')

 <video width="640" height="480" controls>
 <source src="/ece445/wiki/soldering/crimping.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 


![](crimp3.jpg ':size=x350')
![](crimp4.jpg ':size=x350')
![](crimp5.jpg ':size=x350')

<!-- ![Board mounted in baseplate](assembly.jpg) -->

Screw in the rest of the panel mount components, connect them to the PCB, and mount the PCB to the enclosure baseplate.

![Panel-mount components screwed in](assembly2.jpg ':size=x300')
![Panel-mount components connected to board](assembly3.jpg ':size=x300')

 <video width="640" height="400" controls>
 <source src="/ece445/wiki/soldering/demo.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 


Demonstrate your encabulator to the TA and be able to answer the following questions:

- In the test sketch, why is it necessary to cast to `int32_t` when reading the potentiometer? (hint: check the [Arduino docs](https://www.arduino.cc/reference/en/language/variables/data-types/int/) for the size of the return type of analogRead)
- Why do we enable the internal pullup on `BUT_PIN`?  What happens if we don't do this? (refer to the schematic)

## Cleanup Checklist and Grading

Complete this checklist to get a full grade for the assignment:

- disconnect and unscrew all panel mounted components and put them in the large bag (except the LED)
- remove board screws and place in large bag
- USBasp programmer placed in small bag
- remove LED from enclosure by cutting off MTA-100 connector with wirecutters

Return the two bags and enclosure to the TA for full points. Make sure that the TA has recorded your name (we can forget sometimes when the lab is busy).

![](salvage.jpg ':size=350')
![](cleanup.jpg ':size=350')

<!-- # Troubleshooting -->

<!-- ## Microcontroller doesn't program -->

<!-- ## Microcontroller programs, but LED doesn't blink -->

<!-- # Appendix -->


## Bill of Materials

### Disposable Board Components

- 3x - [WM4200 2 pin header](https://www.digikey.com/en/products/detail/molex/0022232021/26667?s=N4IgTCBcDaIO4FsAsYAMqQF0C%2BQ)
- 1x - [WM4201 3 pin header](https://www.digikey.com/en/products/detail/molex/0022232031/26669?s=N4IgTCBcDaIO4FsAsYAMBGAtAOwCYgF0BfIA)
- 1x - [ISP 6 pin programming header](https://www.digikey.com/en/products/detail/61200621621/732-5394-ND/4846913?itemSeq=363606694) [Alt](https://www.digikey.com/en/products/detail/adam-tech/BHR-06-VUA/10414837)
- 1x - [ATTiny85](https://www.digikey.com/en/products/detail/microchip-technology/ATTINY85V-10SU/735472)
- 1x - [MBR0520 diode](https://www.digikey.com/en/products/detail/micro-commercial-co/MBR0520-TP/717250)
- 1x - [LM1117 regulator](https://www.digikey.com/en/products/detail/on-semiconductor/LM1117MPX-50NOPB/13559882)
- 1x - [Panel mount LED](https://www.digikey.com/en/products/detail/SSI-LXH600ID-150/67-1188-ND/145088?itemSeq=368284951)
- 1x - [MTA-100 2 pin connector](https://www.digikey.com/en/products/detail/3-640440-2/A31080-ND/698278?itemSeq=373904068)
- 3x - 0805 1k resistor
- 2x - 0805 1uF capacitor


### Reusable Kit Components

- [MTA-100 2 pin connector](https://www.digikey.com/en/products/detail/3-640440-2/A31080-ND/698278?itemSeq=373904068)
- [MTA-100 3 pin connector](https://www.digikey.com/en/products/detail/3-640440-3/A31081-ND/698279?itemSeq=373904080)
- [Polycase enclosure](https://www.polycase.com/sn-25)
- [Panel mount potentiometer](https://www.digikey.com/en/products/detail/P160KNP-0QC15B10K/987-1731-ND/5957475?itemSeq=367898586)
- [Panel mount button](https://www.digikey.com/en/products/detail/40-1673-01/502PB-ND/2257?itemSeq=368286331)
- [Panel mount barrel](https://www.digikey.com/en/products/detail/PJ-005A/CP-5-ND/165838?itemSeq=369606178)
